const Discord = require('discord.js');
const client = new Discord.Client();

const config = require('./config.json');

client.on('ready', () => {
    console.log("-+ Raiding server " + client.guilds.get(config.serverid).name + " +-")
    console.log("-+ Channel Raided " + client.guilds.get(config.serverid).channels.get(config.channelid).name + " +-")
    console.log("Raid Started!\nTo end it press Crtl+C on your keyboard.")
    client.setInterval(()=> {
        client.guilds.get(config.serverid).channels.get(config.channelid).send(config.message);
    },config.rate);
});

client.login(config.token2);